using System;

namespace Macro.Common
{
    public interface IEntity
    {
        Guid Id { get; set; }
        bool IsDeleted { get; set; }
        DateTimeOffset CreateDate { get; set; }  
  }
}