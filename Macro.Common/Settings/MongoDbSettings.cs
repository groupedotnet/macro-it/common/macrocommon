namespace Macro.Common.Settings
{
    public class MongoDbSettings
    {
        //init à n'est pas changé ulteriement
        public string Host { get; init; }
        public int Port { get; init; }
        public string ConnectionString => $"mongodb://{Host}:{Port }";
    }
}