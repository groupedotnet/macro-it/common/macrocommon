using System.Linq.Expressions;
using MongoDB.Driver;

namespace Macro.Common.MongoDB
{

    public class MongoRepository<T> : IRepository<T> where T : IEntity
    {

        private readonly IMongoCollection<T> itemsCollection;
        private readonly FilterDefinitionBuilder<T> filterBuilder = Builders<T>.Filter;
        public MongoRepository(IMongoDatabase database, string collectionsName)
        {
            itemsCollection = database.GetCollection<T>(collectionsName);
        }
        public async Task<IReadOnlyCollection<T>> GetAllAsync()
        {
            FilterDefinition<T> filter = filterBuilder.Eq(entity => entity.IsDeleted, false);
            return await itemsCollection.Find(filter).ToListAsync();
        }
         public async Task<IReadOnlyCollection<T>> GetAllAsync(Expression<Func<T, bool>> filter)
        {
             return await itemsCollection.Find(filter).ToListAsync();
        }
        public async Task<T> GetAsync(Guid id)
        {
            FilterDefinition<T> filter = filterBuilder.Eq(entity => entity.Id, id)
            & filterBuilder.Eq(entity => entity.IsDeleted, false);
            return await itemsCollection.Find(filter).FirstOrDefaultAsync();
        }
         public async Task<T> GetAsync(Expression<Func<T, bool>> filter)
        {
           return await itemsCollection.Find(filter).FirstOrDefaultAsync();
        }
        public async Task CreateAsync(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            await itemsCollection.InsertOneAsync(item);
        }
        public async Task UpdateAsync(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            FilterDefinition<T> filter = filterBuilder.Eq(existEntity => existEntity.Id, item.Id)
            & filterBuilder.Eq(entity => entity.IsDeleted, false); ;
            await itemsCollection.ReplaceOneAsync(filter, item);
        }
        public async Task RemoveAsync(Guid id)
        {
            FilterDefinition<T> filter = filterBuilder.Eq(entity => entity.Id, id);
            await itemsCollection.DeleteOneAsync(filter);
        }
    }
}